import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json

def get_photo(city, state):
    params = {
        "per_page": 1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    picture_url = content["photos"][0]["src"]["original"]

    return {"picture_url": picture_url}

def get_weather_data(city, state):
    url_lat_lon = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}'
    response_lat_lon = requests.get(url_lat_lon)
    content = json.loads(response_lat_lon.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]


    url_weather = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial'
    response = requests.get(url_weather)
    content2 = json.loads(response.content)
    temp = content2["main"]["temp"]
    description = content2["weather"][0]["description"]
    try:
        return {
            "temp": temp,
            "description": description
            }
    except:
        return {
            "temp": None,
            "description": None,
            }
